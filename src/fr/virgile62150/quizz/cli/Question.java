package fr.virgile62150.quizz.cli;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fr.virgile62150.quizz.lib.lib_autre;

public class Question {
	
	private Map<Integer, Option> possibilites = new HashMap<>();
	private Option reponse;
	private int id_reponse;
	private String question;
	private String annecdote;
	private long id_question;
	
	
	public Question(JSONObject q) {
		question = (String) q.get("question");
		id_question = (long) q.get("id");
		JSONArray reps = (JSONArray) q.get("propositions");
		annecdote = (String) q.get("anecdote");
		for (Object rep :  reps) {
			String elt = (String) rep;
			Option rp = new Option((String) rep);
			possibilites.put(possibilites.size()+1,rp);
			if (elt.equals((String) q.get("réponse"))) {
				reponse = rp;
				id_reponse = possibilites.size();
			}
		}

		
	}
	
	// Runner des questions en CLI
	public void QuestionRunner() {
		
		System.out.println(toString());
		String entree = lib_autre.keyKeyboardLine("Entrez la réponse ou son numéro");
		double point = 1;
		
		while (!isTheCorrectAnswer(entree)) {
			point = point /2;
			System.out.println(entree+" n'est pas la bonne réponse. Essayez encore !");
			entree = lib_autre.keyKeyboardLine("Entrez la réponse");
		}
		Quizz.addAPoint(point);
	}
	
	public Map<Integer, Option> getReponses() {
		return possibilites;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public String toString() {
		StringBuilder stb = new StringBuilder("Question : "+question+"\nRéponses Possibles : \n");
		
		for (Map.Entry<Integer, Option> elt : possibilites.entrySet()) {
			Option rep = elt.getValue();
			int id = elt.getKey();
			stb.append(" "+id+" - "+rep.toString()+"\n");
		}
		
		return stb.toString();
	}
	
	public boolean isTheCorrectAnswer(String str) {
		boolean vrai = str.equals(reponse.toString()) || str.equals(Integer.toString(id_reponse));
		if (vrai) {
			System.out.println(annecdote+"\n");
		}
		
		
		return vrai;
	}
	
	public long getIdQuestion() {
		return this.id_question;
	}
	
}
