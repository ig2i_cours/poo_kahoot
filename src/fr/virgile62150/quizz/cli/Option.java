package fr.virgile62150.quizz.cli;

public class Option {
	private String libelle;
	
	public Option(String obj) {
		libelle = obj;
		//System.out.println(obj);
	}
	
	public String getOpt() {
		return libelle;
	}
	
	public String toString() {
		return libelle;
	}
}
