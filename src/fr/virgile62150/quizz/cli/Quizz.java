package fr.virgile62150.quizz.cli;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import fr.virgile62150.quizz.lib.*;

public class Quizz {
	
	private ArrayList<Question> questions_debutant = new ArrayList<>();
	private ArrayList<Question> questions_avances = new ArrayList<>();
	private static ArrayList<Categorie> liste_categories = new ArrayList<>();
	private Categorie categorie;
	
	private static double score = 0;
	
	private JSONObject quizz;
	
	public Quizz(String filename) throws FileNotFoundException {
		ReadFile fic = new ReadFile(filename);
		String content = fic.read();
		
		quizz = (JSONObject) JSONValue.parse(content);
		
		parseEasy();
		
		String theme = (String) ((JSONObject)quizz.get("quizz")).get("thème");
		addCategorie(theme);
	}
	
	private void addCategorie(String theme) {
		boolean present = false;
		for (Categorie c : liste_categories) {
			if (c.getLibelle().equals(theme)) {
				present = true;
			}
		}
		if (!present) {
			liste_categories.add(new Categorie(theme));
		}
	}
	
	private void parseEasy() {
		
		JSONArray question_easy = ((JSONArray)((JSONObject)((JSONObject)quizz.get("quizz")).get("fr")).get("débutant")) ;
		for (Object o : question_easy) {
			JSONObject q = (JSONObject) o;
			questions_debutant.add(new Question(q));
		}
	}
	
	public static void addAPoint(double point) {
		score+=point;
	}
	
	public double PlayTheQuizz() {
		for (Question q : questions_debutant) {
			q.QuestionRunner();
		}
		return Quizz.score;
	}
	
	public static ArrayList<String> liste_categories() {
		ArrayList<String> strs = new ArrayList<>();
		for (Categorie c : liste_categories) {
			strs.add(c.getLibelle());
		}
		
		return strs;
	}
	

}
