package fr.virgile62150.quizz.cli;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Master {
	private static ArrayList<Quizz> liste_quizz = new ArrayList<>();
	private static ArrayList<Categorie> liste_categories = new ArrayList<>();
	
	public Master() {
		try {
			ReadAllQuizzes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void play() {
		for (Quizz q : liste_quizz) {
			q.PlayTheQuizz();
		}
	}
	
	private void ReadAllQuizzes() throws IOException {
		ArrayList<String> path_list = new ArrayList<>();
		try (Stream<Path> paths = Files.walk(Paths.get("./quizzes"))) {
		    paths
		        .filter(Files::isRegularFile)
		        .forEach(
		        		(file) -> {
		        	path_list.add(file.toString());
		        });
		}
		
		for(String path : path_list) {
			liste_quizz.add(new Quizz(path));
		}
		
				
	}
	
}
