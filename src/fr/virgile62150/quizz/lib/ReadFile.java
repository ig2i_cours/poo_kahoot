package fr.virgile62150.quizz.lib;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {
	
	File monFichier;
	
	public ReadFile(String filename) {
			monFichier = new File(filename);	
	}
	
	public String read() throws FileNotFoundException {
		Scanner monScanner = new Scanner(monFichier);
		StringBuilder string = new StringBuilder("");
		
		while(monScanner.hasNextLine()) {
			string.append(monScanner.nextLine());
		}
		monScanner.close();
		
		return string.toString();
	}
}
