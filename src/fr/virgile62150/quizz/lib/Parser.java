package fr.virgile62150.quizz.lib;

import org.json.simple.*;

public class Parser {
	
	private Object monJson;
	
	public Parser(String elt) {
		monJson = JSONValue.parse(elt);
	}
	
	public void Print() {
		System.out.println(monJson);
	}
	
	public JSONObject getJson() {
		return (JSONObject) monJson;
	}
	
}
